﻿using UnityEngine;
using System.Collections;

public class Agent : MonoBehaviour {

    public float speed = 1f;

    private Edge chosenEdge;

    public bool isMoving { get; private set; }

    public Field currentField {
        get { return GridManager.Instance.GetField(transform.position.x, transform.position.y); } }

    private SpriteRenderer _glow;
    private SpriteRenderer glow
    {
        get
        {
            if (_glow == null)
                _glow = transform.FindChild("Glow").GetComponent<SpriteRenderer>();

            return _glow;
        }
    }

    private Charge _charge;
    public Charge charge
    {
        get {
            return _charge;
        }
        set {
            _charge = value;

            if (charge != null)
            {
                _charge.transform.parent = transform;
                _charge.transform.localPosition = Vector3.zero;
                glow.color = _charge.color;
            }
            else
            {
                glow.color = Color.white;
            }
        }
    }

    private Direction _chosenDirection;
    public Direction chosenDirection {
        get
        {
            return _chosenDirection;
        }
        set
        {
            if (_chosenDirection == value)
                return;

            _chosenDirection = value;

            if (chosenEdge != null)
            {
                chosenEdge.Deactivate();
                chosenEdge = null;
            }

            chosenEdge = currentField.GetEdge(chosenDirection);
                
            if (chosenEdge != null)
            {
                bool shouldAnimateForward = 
                    _chosenDirection == Direction.Right || 
                    _chosenDirection == Direction.Down;

               chosenEdge.Activate(shouldAnimateForward);

            }
        }
    }

    public void Move(Direction chosenDirection)
    {
        StartCoroutine(MoveCoroutine(chosenDirection));
    }

    private IEnumerator MoveCoroutine(Direction chosenDirection)
    {
        GridManager.Instance.GetField(transform.position.x, transform.position.y).OnExitAgent();

        isMoving = true;

        Vector3 finalPosition = transform.position + chosenDirection.vector;

        while (Vector3.Distance(transform.position, finalPosition) > 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, finalPosition, speed * Time.deltaTime);
            yield return 0;
        }

        GridManager.Instance.GetField(finalPosition.x, finalPosition.y).OnEnterAgent(this);

        if (chosenEdge != null)
            chosenEdge.Deactivate();

        isMoving = false;
    }
}
