﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragDropInput : MonoBehaviour {

    private Vector2 initialDragPosition;

    private Agent cachedAgent;

    private void Awake()
    {
        cachedAgent = GetComponent<Agent>();
    }

    private void OnMouseDown()
    {
        if (LevelManager.Instance.currentState == LevelManager.State.Executing)
            return;

        initialDragPosition = Input.mousePosition;
    }

    private void OnMouseDrag()
    {
        if (LevelManager.Instance.currentState == LevelManager.State.Executing)
            return;

        cachedAgent.chosenDirection = getDragDirection(Input.mousePosition);
    }

    private void OnMouseUp()
    {
        if (LevelManager.Instance.currentState == LevelManager.State.Executing)
            return;

        LevelManager.Instance.TryExecute();
    }

    private Direction getDragDirection(Vector2 currentPos)
    {
        Vector2 vector = currentPos - initialDragPosition;

        if (Mathf.Abs(vector.x) > Mathf.Abs(vector.y))
        {
            if (vector.x > 0)
                return Direction.Right;
            else
                return Direction.Left;
        }
        if (Mathf.Abs(vector.y) > Mathf.Abs(vector.x))
        {
            if (vector.y > 0)
                return Direction.Up;
            else
                return Direction.Down;
        }
        return null;

    }

}
