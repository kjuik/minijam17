﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreen : MonoBehaviour {

	void Update ()
    {
        if (Input.anyKeyDown || Input.GetMouseButtonDown(0))
            StartCoroutine(LoadNewLevelCoroutine());
    }

    private IEnumerator LoadNewLevelCoroutine()
    {
        FadeToWhite.Instance.FadeOut();
        yield return new WaitForSeconds(FadeToWhite.Instance.FadeTime);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
