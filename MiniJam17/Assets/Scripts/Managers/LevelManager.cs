﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

public class LevelManager : MonoBehaviour {

    public static LevelManager Instance;

    public enum State
    {
        Waiting, Executing
    }
    public State currentState;

    private Agent[] agents;

    void Awake()
    {
        Instance = this;
        agents = FindObjectsOfType<Agent>();
    }

    void Update () {
        if (currentState == State.Executing)
            UpdateExecution();
	}

    public void TryExecute()
    {
        if (currentState == State.Waiting && ValidateAgents())
        {
            currentState = State.Executing;
            agents[0].Move(agents[0].chosenDirection);
            agents[1].Move(agents[1].chosenDirection);
        }
    }

    private bool ValidateAgents()
    {
        for (int i=0; i < agents.Length; ++i)
        {
            if (!ValidateAgent(agents[i]))
                return false;
        }
        return true;
    }

    private bool ValidateAgent(Agent a)
    {
        if (a.chosenDirection == null)
            return false;

        Field f = GridManager.Instance.GetField(a.transform.position.x, a.transform.position.y);

        Edge e = f.GetEdge(a.chosenDirection);

        if (e == null)
            return false;

        return e.CanPass(a);
    }

    private void UpdateExecution()
    {
        foreach(Agent a in agents)
            if (a.isMoving)
                return;

        if (agents[0].transform.position == agents[1].transform.position)
            StartCoroutine(LoadNewLevel());

        foreach (Agent a in agents)
            a.chosenDirection = null;

        currentState = State.Waiting;
    }

    IEnumerator LoadNewLevel()
    {
        float timer = 0f;
        while (timer < 2f) {
            foreach (Agent a in agents)
                a.transform.localScale *= (1 + 2 * Time.deltaTime);

            yield return 0f;
            timer += Time.deltaTime;
       }

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ResetLevel()
    {
        StartCoroutine(ResetCoroutine());
    }

    private IEnumerator ResetCoroutine()
    {
        FadeToWhite.Instance.FadeOut();
        yield return new WaitForSeconds(FadeToWhite.Instance.FadeTime);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
