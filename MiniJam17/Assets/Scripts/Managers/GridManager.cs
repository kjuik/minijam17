﻿using UnityEngine;
using System.Linq;

public class GridManager : MonoBehaviour
{
    public static GridManager Instance { get; private set; }

    public Field[,] fields;

    private int minX;
    private int minY;

    void Awake()
    {
        Instance = this;
        organizeFields();
        organizeEdges();
    }

    public Field GetField(float x, float y)
    {
        return fields[(int)x - minX, (int)y - minY];
    }

    private void organizeFields()
    {
        Field[] foundFields = FindObjectsOfType<Field>();

        minX = (int)(foundFields.Min(x => x.transform.position.x));
        minY = (int)(foundFields.Min(x => x.transform.position.y));

        int maxX = (int)(foundFields.Max(x => x.transform.position.x));
        int maxY = (int)(foundFields.Max(x => x.transform.position.y));

        fields = new Field[maxX - minX + 1, maxY - minY + 1];

        foreach (Field f in foundFields)
        {
            fields[(int)f.transform.position.x - minX, (int)f.transform.position.y - minY] = f;
            f.name = f.GetType().Name + " [" + ((int)f.transform.position.x - minX) + ":" + ((int)f.transform.position.y - minY) + "]";
            f.transform.parent = transform;
        }
    }

    private void organizeEdges()
    {
        Edge[] foundEdges = FindObjectsOfType<Edge>();

        foreach (Edge e in foundEdges)
        {
            Field f = fields[(int)e.transform.position.x - minX, (int)e.transform.position.y - minY];

            Field f2 = null;

            switch ((int)(e.transform.rotation.eulerAngles.z))
            {
                case 0:
                    f.edgeRight = e;
                    f2 = fields[(int)e.transform.position.x - minX + 1, (int)e.transform.position.y - minY];
                    f2.edgeLeft = e;
                    break;
                case 90:
                    f.edgeUp = e;
                    f2 = fields[(int)e.transform.position.x - minX, (int)e.transform.position.y - minY + 1];
                    f2.edgeDown = e;
                    break;
                case 180:
                    f.edgeLeft = e;
                    f2 = fields[(int)e.transform.position.x - minX - 1, (int)e.transform.position.y - minY];
                    f2.edgeRight = e;
                    break;
                case 270:
                    f.edgeDown = e;
                    f2 = fields[(int)e.transform.position.x - minX, (int)e.transform.position.y - minY - 1];
                    f2.edgeUp = e;
                    break;
            }

            e.name = e.GetType().Name + " [" + (int)(f.transform.position.x - minX) + ":" + (int)(f.transform.position.y - minY) + "]";
            e.transform.parent = transform;
        }
    }
}
