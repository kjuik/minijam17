﻿using UnityEngine;

public class Edge : MonoBehaviour {

    private Animator _cachedAnimator;
    private Animator cachedAnimator {
        get {
            if (_cachedAnimator == null)
                _cachedAnimator = GetComponentInChildren<Animator>();
            return _cachedAnimator;
        }
    }

    public virtual bool CanPass(Agent a)
    {
        return true;
    }

    public void Activate(bool forward)
    {
        cachedAnimator.SetTrigger(forward ? "Forward" : "Backward");
    }

    public void Deactivate()
    {
        cachedAnimator.SetTrigger("Default");
    }

}
