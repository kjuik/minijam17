﻿
public class EdgeConditional : Edge {

    public bool chargeValue;

    public override bool CanPass(Agent a)
    {
        if (a.charge == null)
            return false;

        return a.charge.value == chargeValue;
    }
}
