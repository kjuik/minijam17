﻿using UnityEngine;

public class Field : MonoBehaviour {

    public Edge edgeUp;
    public Edge edgeDown;
    public Edge edgeLeft;
    public Edge edgeRight;

    public Charge charge;

    void Awake()
    {
        if (charge == null)
            charge = GetComponentInChildren<Charge>();
    }

    public virtual void OnEnterAgent(Agent a)
    {
        if (charge != null)
        {
            charge.OnEnterAgent(a);
            charge = null;
        }
    }

    public virtual void OnExitAgent() { }

    public Edge GetEdge(Direction direction)
    {
        if (direction == Direction.Down)
            return edgeDown;
        else if (direction == Direction.Up)
            return edgeUp;
        else if (direction == Direction.Left)
            return edgeLeft;
        else if (direction == Direction.Right)
            return edgeRight;

        return null;
    }
}
