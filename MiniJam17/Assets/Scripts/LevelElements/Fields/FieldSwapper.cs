﻿using System.Collections.Generic;

public class FieldSwapper : Field {

    private static List<FieldSwapper> Instances = new List<FieldSwapper>();

    void OnEnable()
    {
        Instances.Add(this);
    }

    void OnDisable()
    {
        Instances.Remove(this);
    }

    private Agent currentAgent;

    public override void OnEnterAgent(Agent agent)
    {
        base.OnEnterAgent(agent);
        currentAgent = agent;

        foreach(FieldSwapper s in Instances) if (s != this)
        {
            if (s.currentAgent != null)
            {
                Charge buffer = currentAgent.charge;
                currentAgent.charge = s.currentAgent.charge;
                s.currentAgent.charge = buffer;
                return;
            }
        }
    }

    public override void OnExitAgent()
    {
        currentAgent = null;
    }
}
