﻿using UnityEngine;

public class Charge : MonoBehaviour {

    public bool value;
    public Color color;

    public void OnEnterAgent(Agent a)
    {
        if (a.charge != null)
        {
            if (a.charge.value != value)
            {
                Destroy(gameObject);
                Destroy(a.charge.gameObject);
            }
            else
            {
                Destroy(a.charge.gameObject);
            }
        }
        else
        {
            transform.parent = a.transform;
            a.charge = this;
        }
    }

}
