﻿using UnityEngine;

public class Direction
{
    public static Direction Up = new Direction(Vector3.up);
    public static Direction Down = new Direction(Vector3.down);
    public static Direction Left = new Direction(Vector3.left);
    public static Direction Right = new Direction(Vector3.right);

    public Vector3 vector { get; private set; }

    private Direction(Vector3 v)
    {
        vector = v;
    }

}
