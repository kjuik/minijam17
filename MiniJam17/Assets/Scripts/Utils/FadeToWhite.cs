﻿using UnityEngine;
using UnityEngine.UI;

public class FadeToWhite : MonoBehaviour
{
    public static FadeToWhite Instance;

    public float FadeTime = 1f;
    public bool fadeInOnStart = true;

    private Image cachedImage;
    private float currentOpacityTarget = 0f;

    void Awake()
    {
        Instance = this;
        cachedImage = GetComponent<Image>();
    }

    private void Start()
    {
        if (fadeInOnStart)
        {
            cachedImage.color = new Color(1f, 1f, 1f, 1f);
            FadeIn();
        }
    }

    public void FadeIn()
    {
        currentOpacityTarget = 0f;
    }

    public void FadeOut()
    {
        currentOpacityTarget = 1f;
    }

    private void Update()
    {
        if (cachedImage.color.a != currentOpacityTarget)
        {
            cachedImage.color = new Color(
                1f, 1f, 1f, 
                Mathf.MoveTowards(
                    cachedImage.color.a, 
                    currentOpacityTarget, 
                    FadeTime * Time.deltaTime
                )
            );
        }
    }
}
